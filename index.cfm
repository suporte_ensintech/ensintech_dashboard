<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<cfprocessingdirective pageencoding = "utf-8">
<title>Protótipo</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet" media=" screen" href="style.css"/>		
<meta http-equiv="refresh" content="60">   
 <link rel = "shortcut icon" type = "imagem/logo.png" id ="img"href = "imagem/logoo.png"/>
</head>
 
<body>
<div id="site">
   
    <cfoutput>
        <cfscript>
            cfcChamados= CreateObject("component", "cfc.chamados");
            chamadosabertos=cfcChamados.get_chamados_abertos();
            exerc= cfcChamados.get_qtd_chamados();
           
          </cfscript>
    <div id="header">
        <img id = "nac"src="imagem\logo.png">
        <h1>Monitoramento</h1>
        <h1 id= "atual">Proxima <br> atualização<br>
            <cfinclude template="contagemregre.cfm">
        </h1><br>
        
    </div>
    <div id="conteudo"><!-- abrimos a div conteudo -->
        <div id="conteudo-left">
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                <script type="text/javascript">
           google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ["", "" ]
            <cfloop query="chamadosabertos">
            ,["#replace(#chamadosabertos.origem# , 'ENSINTECH_' , '')# ",#chamadosabertos.quant# ]
            </cfloop>
        ]);
        var options = {
          chart: {
            title: 'Total de chamados abertos #exerc.quant#',
            subtitle: '',
          },
          bars: 'horizontal' // Required for Material Bar Charts.
        };
        var chart = new google.charts.Bar(document.getElementById('barchart_material'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    
                </script>
              </head>
              <body>
                <div id="barchart_material"></div>
              </body>
        </div>
        <div id="conteudo-right">
            <center><h2 id = "cartao">CARTÕES PARA PROCESSAR</h2><center>
        
                <cfinvoke   webservice = "http://ensintech.sp.senac.br/api/cr.cfc?wsdl" 
                method = "getNumCRFila"
                returnVariable = "v_numero_cr">
                <cfoutput>
                   <!--- <cfset v_numero_cr = 10 /> --->
                <cfif v_numero_cr eq 0 >   
                    
                    <font size="20px" face="verdana"color="gray" margin-left="-20px">#v_numero_cr#</font> 
            
                <cfelse>
                    <audio src="audio.mp3"autoplay preload="auto">  </audio>
                        <font size="20px" face="verdana" color="red">#v_numero_cr#</font>
                   
                </cfif>   
                </center>
                </cfoutput>
        </div>
        <div id="conteudo-bottom">
            <center><h2 id = "cartao">FILA <br> DOUBLE CHECK</h2><center>
                <cfinvoke   webservice = "http://ensintech.sp.senac.br/api/cr.cfc?wsdl" 
            method = "getNumCRFilaDoubleChk"
            returnVariable = "v_numero_double_chk">

        <cfoutput>
        <cfif v_numero_double_chk eq 0 >   
          
           <font size="20px" face="verdana"color="gray" margin-left="-15">#v_numero_double_chk#</font>
           
            <cfelse>
                <audio src="audio.mp3"autoplay preload="auto"></audio>
                <font size="20px" face="verdana" color="red">#v_numero_double_chk#</font> 
        
        </cfif>
</center>
</cfoutput>
        </div>
    </div><!-- aqui fechamos a div conteudo -->

    </cfoutput>
</div>
</body>
</html>