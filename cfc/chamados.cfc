<cfcomponent>
    <!--- Aqui estamos criando uma função que ira retorno o numero total de chamados.
              para você criar mais funções neste mesmo arquivo basta copiar e colar o bloco da cffunntion,
              alterandoo nome da função. --->
    <cffunction name ="get_chamados_abertos" returntype = "query" access="remote">
    <cfquery name ="chamadosabertos"datasource="dsn_suavia">
		SELECT origem , COUNT(*) AS quant 
			  FROM tb_servicedesk_chamados  
			 WHERE (status  = "A") 
			GROUP BY origem;
        </cfquery>
        <cfreturn chamadosabertos>
    </cffunction>

    <cffunction name="get_qtd_chamados" returntype="query" access="remote" >	
        <!-- Realiza a consulta ao banco de dados-->	
    <cfquery name="qryorg" datasource="dsn_suavia">
      select  origem ,count(status = "A") as quant 
             FROM tb_servicedesk_chamados
             group by status
             having quant <2000;
    </cfquery>
    <cfreturn qryorg>
</cffunction>
</cfcomponent>